import os
from shutil import rmtree

test_dir = 'test/data'
tmp_dir = os.path.join(test_dir, 'tmp')

try:
    rmtree(tmp_dir)
    os.mkdir(tmp_dir)
except OSError:
    os.mkdir(tmp_dir)
