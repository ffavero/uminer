import unittest
import os
from pysam import AlignmentFile, VariantFile
from uminer.pysammisc import parse_region, parse_record

class TestPySam(unittest.TestCase):

    def test_VcfRead(self):
        file_in = os.path.join('test', 'data', 'input',
            'umi_calls.vcf.gz')
        with VariantFile(file_in) as vcf_in:
            for res in vcf_in.fetch():
                zz = parse_record(res)
                self.assertFalse(zz['ref'] in zz['alt'])

    def test_SamRead(self):
        file_in = os.path.join('test', 'data', 'input',
            'raw_umi.bam')
        counts = {'A': 0, 'C': 0, 'G': 0, 'T': 0}
        umis = dict()
        with AlignmentFile(file_in) as sam_in:
            for i in parse_region(sam_in, '1', 1720619):
                umi_tag = i['umi']
                if umi_tag not in umis:
                    umis[umi_tag] = {'A': 0, 'C': 0, 'G': 0, 'T': 0}
                if i['quality_map'] >= 60:
                    rel_pos = i['relative_position']
                    target = i['seq'][rel_pos]
                    qual = i['qual'][rel_pos]
                    if qual > 0:
                        umis[umi_tag][target] += 1
        print umis

if __name__ == '__main__':
    unittest.main()
