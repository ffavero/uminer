from __future__ import division


def get_references(fasta, chrom, pos, ref, alt, flank=2):
    ref_len = len(ref)
    alt_len = len(alt)
    len_max = max(ref_len, alt_len)
    diff_len = len_max - min(ref_len, alt_len)
    min_len = len_max - diff_len + flank + flank
    start_pos = pos - 1 - flank
    end_pos = pos + len_max - 1 + flank + diff_len
    ref_fa = fasta.fetch(chrom, start_pos, end_pos)
    alt_fa = ref_fa[0:flank] + alt + ref_fa[ref_len + flank:len(ref_fa)]
    return {'ref': ref_fa[0:min_len], 'alt': alt_fa[0:min_len],
            'start': start_pos + 1, 'len': min_len}


def parse_segment(aligned_segment, position, header):
    start = aligned_segment.reference_start
    seq = aligned_segment.query_sequence
    qual = aligned_segment.query_qualities
    name = aligned_segment.query_name
    try:
        umi = aligned_segment.get_tag("BX")
    except KeyError:
        umi = name.split('_')[-1]
    chrom_id = aligned_segment.reference_id
    chrom = header['SQ'][chrom_id]
    map = aligned_segment.mapping_quality
    rel_pos = position - start - 1
    return({'umi': umi, 'chromosome': chrom,
            'position': start, 'relative_position': rel_pos,
            'quality_map': map, 'seq': seq, 'qual': qual})


def parse_region(alignment_file, chromosome, position):
    for read in alignment_file.fetch(chromosome, position, position + 1):
        res = parse_segment(read, position, alignment_file.header)
        yield(res)


def parse_record(variant_record):
    chrom = variant_record.chrom
    pos = variant_record.pos
    ref = variant_record.ref
    alt = variant_record.alleles[1:]
    return({'chrom': chrom, 'pos': pos,
            'ref': ref, 'alt': alt})


def get_quants(x, quants):
    diff_quants = [abs(i - x) for i in quants]
    return(diff_quants.index(min(diff_quants)))


def count_samples_quants(samples, quants):
    quants_n = [0 for i in quants]
    for sample in samples:
        try:
            try:
                af_i = samples[sample]['AF']
                if isinstance(af_i, tuple):
                    freq = max(samples[sample]['AF'])
                else:
                    freq = samples[sample]['AF']
            except KeyError:
                ao_i = max(samples[sample]['AO'])
                dp_i = samples[sample]['DP']
                if ao_i is None or dp_i is None:
                    freq = None
                else:
                    freq = ao_i / dp_i
            except KeyError:
                raise Exception('Unsupported alternate allele format')
            if freq:
                quants_n[get_quants(freq, quants)] += 1
        except TypeError as e:
            raise e
    return quants_n
