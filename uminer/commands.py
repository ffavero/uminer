#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pysam import AlignmentFile, VariantFile, FastaFile
import argparse
from uminer import __version__
from uminer.misc import umi_logging
from uminer.pysammisc import parse_region, parse_record, \
    count_samples_quants, get_references
from uminer.umi import umi_families, stats_umis


def main():
    '''
    Execute the function with args
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--fasta',  help='Fasta reference file',
                        required=True)
    parser.add_argument('-b', '--bam',  help='raw/non-dedup BAM file',
                        required=True)
    parser.add_argument('-v', '--vcf',  help='VCF file with variant to parse',
                        required=True)
    parser.add_argument('-d', '--vcf_db',
                        help='VCF file with batch/population calls',
                        required=True)
    parser.add_argument('-s', '--family-size',  help='Minimal UMI family size',
                        default=2, type=int)
    parser.add_argument('-r', '--ratio-consensus',
                        help=('Minimal consensus within '
                              'the UMI family to support the variant'),
                        default=1, type=float)
    parser.add_argument('-m', '--min-map',
                        help='Minimum map quality of alignment',
                        default=40, type=int)
    parser.add_argument('-q', '--min-qual',
                        help='Minimum base quality of the alt read',
                        default=14, type=int)

    args = parser.parse_args()

    # with VariantFile(args.vcf) as vcf_in:
    #    for res in vcf_in.fetch():
    #        vcf_line = parse_record(res)
    #quants = (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1)
    quants = [i/100.0 for i in range(0, 105, 5)]
    with VariantFile(args.vcf) as vcf_in, VariantFile(args.vcf_db) as vcf_db, \
            FastaFile(args.fasta) as ref_fa, AlignmentFile(args.bam) as sam_in:
        vcf_in.header.info.add(
            'UMI_FTOT', None, 'Integer',
            'Total number of UMI families represented in the position')
        vcf_in.header.info.add(
            'UMI_FBIG', None, 'Integer',
            ('Number of UMI family bigger then %i '
             'represented in the  candidate position' % args.family_size))
        vcf_in.header.info.add(
            'UMI_FREF', None, 'Integer',
            ('Number of UMI families bigger then %i '
             'representing the reference allele in '
             'the  candidate position' % args.family_size))
        vcf_in.header.info.add(
            'UMI_FALT', None, 'Integer',
            ('Number of UMI families bigger then %i '
             'representing the alternate allele in '
             'the  candidate position' % args.family_size))
        vcf_in.header.info.add(
            'UMI_FFREQ', None, 'Float',
            ('Frequency of UMI families bigger then %i '
             'supporting the alternate allele' % args.family_size))
        vcf_in.header.info.add(
            'UMI_REF', None, 'Integer',
            ('Number of UMI families of any size '
             'representing the reference allele in '
             'the candidate position'))
        vcf_in.header.info.add(
            'UMI_ALT', None, 'Integer',
            ('Number of UMI families of any size '
             'representing the alternate allele in '
             'the  candidate position'))
        vcf_in.header.info.add(
            'UMI_FREQ', None, 'Float',
            ('Frequency of UMI families of any size '
             'supporting the alternate allele'))
        vcf_in.header.info.add(
            'VAR_BATCH', None, 'Float',
            'Histogram of number of samples vs allele ferquency bins: %s' %
                               ', '.join(map(str, quants)))
        with VariantFile('-', 'w', header=vcf_in.header) as vcf_out:
            for res in vcf_in.fetch():
                vcf_line = parse_record(res)
                ref = vcf_line['ref']
                alt = vcf_line['alt']
                chrom = vcf_line['chrom']
                pos = vcf_line['pos']
                quant_i = [0 for i in quants]
                db_lines = vcf_db.fetch(chrom, pos - 1, pos)
                for line in db_lines:
                    if chrom == line.chrom:
                        samples = line.samples
                        qq = count_samples_quants(samples, quants)
                        quant_i = [
                            qq[i] + quant_i[i] for i in xrange(len(quant_i))]
                res.info['VAR_BATCH'] = quant_i
                if isinstance(alt, str):
                    alt = alt
                else:
                    alt = alt[0]
                ref_info = get_references(ref_fa, chrom, pos, ref, alt, 2)
                aligns = parse_region(sam_in, chrom, ref_info['start'])
                fam = umi_families(aligns, ref_info['ref'],
                                   ref_info['alt'], args.min_map,
                                   args.min_qual)
                stats = stats_umis(fam, args.family_size,
                                   args.ratio_consensus)
                res.info['UMI_FTOT'] = stats[0]
                res.info['UMI_FBIG'] = stats[1]
                res.info['UMI_FREF'] = stats[2]
                res.info['UMI_FALT'] = stats[3]
                res.info['UMI_FFREQ'] = stats[4]
                res.info['UMI_REF'] = stats[5]
                res.info['UMI_ALT'] = stats[6]
                res.info['UMI_FREQ'] = stats[7]
                # else:
                #     stats = ['NA', 'NA', 'NA', 'NA', 'NA']
                vcf_out.write(res)


if __name__ == "__main__":
    main()
