import os
import logging
import datetime
import random
import string


def generate_uid(n=4):
    random_str = ''.join(random.choice(
        string.ascii_uppercase + string.digits) for _ in range(n))
    random_str = '%s_%s' % (
        datetime.datetime.now().strftime("%y%m%d%H%M%S.%f"), random_str)
    return random_str


class umi_logging:

    def __init__(self, name, level):
        self.__name__ = name
        self.log = logging.getLogger(name)
        formatter = logging.Formatter(
            '%(levelname)s : %(asctime)s : %(message)s')
        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(formatter)
        self.log.setLevel(level)
        self.log.addHandler(streamHandler)
