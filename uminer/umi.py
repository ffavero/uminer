from __future__ import division


def umi_families(bam_reads, ref, alt, map, bqual):
    ref_len = len(ref)
    alt_len = len(alt)
    umis = dict()
    for i in bam_reads:
        umi_tag = i['umi']
        if umi_tag not in umis:
            umis[umi_tag] = {'ref': 0, 'alt': 0, 'tot': 0}
        if i['quality_map'] >= map:
            rel_pos = i['relative_position']
            try:
                qual = i['qual'][rel_pos]
            except IndexError:
                # reads does not overalp with hte position
                qual = 0
            if qual > bqual:
                umis[umi_tag]['tot'] += 1
                if ref == i['seq'][rel_pos:rel_pos + ref_len]:
                    umis[umi_tag]['ref'] += 1
                elif alt == i['seq'][rel_pos:rel_pos + alt_len]:
                    umis[umi_tag]['alt'] += 1
    return umis


def stats_umis(families, min_tot, min_cons):
    tot_fam = 0
    big_fam = 0
    ref_fam = 0
    alt_fam = 0
    umi_ref = 0
    umi_alt = 0
    for key in families:
        tot_fam += 1
        family = families[key]
        if family['tot'] >= min_tot:
            big_fam += 1
            if family['ref'] / family['tot'] >= min_cons:
                ref_fam += 1
            elif family['alt'] / family['tot'] >= min_cons:
                alt_fam += 1
        if family['ref'] / family['tot'] >= min_cons:
            umi_ref += 1
        elif family['alt'] / family['tot'] >= min_cons:
            umi_alt += 1
    if big_fam > 0:
        alt_freq = alt_fam / big_fam
    else:
        alt_freq = 0
    if umi_tot > 0:
        umi_alt_freq = umi_alt / umi_tot
    else:
        umi_alt_freq = 0

    return [tot_fam, big_fam, ref_fam, alt_fam, alt_freq, umi_ref, umi_alt, umi_alt_freq]
