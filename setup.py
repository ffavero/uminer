from setuptools import setup
import sys

from uminer import __version__

VERSION = __version__.VERSION
DATE = __version__.DATE
AUTHOR = __version__.AUTHOR
MAIL = __version__.MAIL
WEBSITE = __version__.WEBSITE

install_requires = ['pysam']

try:
    import argparse
except ImportError:
    install_requires.append('argparse')

setup(
    name='uminer',
    version=VERSION,
    description='collect UMI information for candidate SNV',
    long_description='',
    author=AUTHOR,
    author_email=MAIL,
    url=WEBSITE,
    license='GPLv3',
    packages=['uminer'],
    entry_points={
        'console_scripts': ['uminer = uminer.commands:main']
    },
    install_requires=install_requires,
    test_suite='test',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
    ],
    keywords='bioinformatics'
)
